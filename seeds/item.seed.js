const mongoose = require('mongoose');
const Item = require('../models/Item.model.js');
const db = require('../db');

const ItemsSeed =[
    {
    productCode: "A001",
    title: "zapatillas",
    image:"no hay imagen" ,
    description: "Zapatillas chulas" ,
    price: 50,
    manufacturer: "bela",
    available: true,
},

{
    productCode: "A002",
    title: "Sudadera",
    image:"no hay imagen" ,
    description: "sudadera chula" ,
    price: 70,
    manufacturer: "bela",
    available: true,
},

{
    productCode: "A003",
    title: "Pantalones",
    image:"no hay imagen" ,
    description: "pantalones chulos" ,
    price: 30,
    manufacturer: "bela",
    available: true,
}
]
   
 
  


mongoose
    .connect(db.DB_URL, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(async () => {
        console.log('Conectado a la base de datos desde el seed...');  // mensaje de conexion exitosa

        const allItem = await Item.find();  // hacemos un abusqueda en la base de datos

        if (allItem.length) {
            /**
             * Ya existen productos, los elimino
             */

            await Item.collection.drop();    // elimino la colección
            console.log('Colección eliminada correctamente...')
        }
    })
    .catch(error => {
        console.log('Error eliminando la colección Footballer', error);
    })
    .then(async () => {
        /**
         * Ahora, después de eliminada la colección, creo de nuevo mis mascotas iniciales.
         */ 

        await Item.insertMany(ItemsSeed);   ///insertamos y creamos una instancia del modelo
        console.log('SUCCESS: items añadidos con éxito...')
    })
    .catch(error => {
        console.log('Error añadiendo el seed', error);
    })
    .finally(() => mongoose.disconnect());