const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const cartSchema = new Schema(
    {
        owner: { type: mongoose.Types.ObjectId, ref: 'Users', required: true },
        products: [{
            productId: { type: mongoose.Types.ObjectId, ref: 'Products' },
            quantity: { type: Number, required: true },
        }],
        totalPrice: { type: Number, required: true, default: 0},
        totalQuantity: { type: Number, required: true, default: 0 },
    },
    { timestamps: true }
);

const Cart = mongoose.model('Carts', cartSchema);

module.exports = Cart;