const express = require('express');
const dotenv = require('dotenv');
dotenv.config();
const path = require('path');
const methodOverride = require('method-override');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const passport = require('passport');
require('./auth');
const db = require('./db');
const indexRoutes = require('./routes/index.routes');
const authRoutes = require('./routes/auth.routes');
const productsRoutes = require('./routes/products.routes');
const cartRoutes = require('./routes/cart.routes');

db.connect();

const PORT = process.env.PORT || 3200;

const server = express();

server.use(session({
    secret: process.env.SECRET, 
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 1 * 24 * 60 * 60 * 1000,
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL }),
}));

server.use(passport.initialize());
server.use(passport.session());

server.use(methodOverride('_method'))
server.use(express.json());
server.use(express.urlencoded({ extended: true }));

server.use(express.static(path.join(__dirname, 'public')));

server.set('views', path.join(__dirname, 'views'))
server.set('view engine', 'hbs');

server.use((req, res, next) => {
    req.isAdmin = false;

    if(!req.isAuthenticated()) {
        return next();
    } else {
        req.isUser = true;
    }

    if (req.user && req.user.role === 'admin') {
        req.isAdmin = true;
    }

    return next();
});

server.use('/', indexRoutes);
server.use('/auth', authRoutes);
server.use('/products', productsRoutes);
server.use('/cart', cartRoutes);

server.use('*', (req, res, next) => {
    const error = new Error('Ruta no encontrada');

    return res.status(404).render('error', {
        message: error.message,
        status: 404,
    });
});

server.use((error, req, res, next) => {
    console.log(error);

    return res.status(error.status || 500).render('error', {
        message: error.message || 'Unexpected error, try again',
        status: error.status || 500,
    });
});

server.listen(PORT, () => {
    console.log(`Server listening on http://localhost:${PORT}`);
})