const express = require('express');
const { isAdmin } = require('../middlewares/auth.middleware');
const { upload, uploadToCloudinary } = require('../middlewares/file.middleware');

const {
    productsGet,
    createGet,
    createPost,
    editGet,
    editPut,
    deleteProduct,
} = require('../controllers/products.controller');

const router = express.Router();

router.get('/', productsGet);

router.get('/create', isAdmin, createGet);
router.post('/create', [isAdmin, upload.single('image'), uploadToCloudinary], createPost);

router.get('/edit/:id', isAdmin, editGet);
router.put('/edit/:id', [isAdmin, upload.single('image'), uploadToCloudinary], editPut);

router.delete('/delete/:id', isAdmin, deleteProduct);

module.exports = router;